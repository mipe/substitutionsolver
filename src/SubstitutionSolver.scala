import scala.io.Source

object SubstitutionSolver {

  def decrypt(mapping: Map[Char, Char], encrypted: String) {
    var decrypted = new String
    for (char <- encrypted.toCharArray) {
      decrypted += { if (mapping.contains(char)) mapping.apply(char) else char }
    }
    println(decrypted)
  }

  def readEncryptedText: String = {
    val source = Source.fromFile("src/encrypted.txt")
    val lines = source.mkString
    source.close
    lines
  }

  def readSubstitutionMap: Map[Char, Char] = {
    val source = Source.fromFile("src/mapping.txt")
    var map = Map.empty[Char, Char]
    for (line <- source.getLines) {
      val tokens = line.split("=").flatMap(x => x.toCharArray)
      map += tokens(0) -> { if (tokens.size == 2) tokens(1) else '_' }
    }
    source.close
    map.toMap
  }

  def main(args: Array[String]) {
    decrypt(readSubstitutionMap, readEncryptedText.toLowerCase)
  }
}